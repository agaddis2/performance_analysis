;========================================================================================
; ACME model ensemble comparison script for individual 4D (time, level, lat, lon) 
; variables at select levels. Outputs pdf, qq plot, and KS statistics.
;
; 9/29/2015
; Abigail L.Gaddis
;=======================================================================================

  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
  load "./stats_functions.ncl"

;-------------------------
; User specified settings
;-------------------------

  ;automatically sized bin options for histogram
  opt = False
  nbins = 25

  ;manual histogram bin size options
;  opt = True
;  min_bin = 284
;  max_bin = 288
;  opt@bin_min = min_bin
;  opt@bin_max = max_bin

  ; variable and level to analyze
;  varname = "T"
;  level = 850
  varname = "U"
;  varname = "V"
  level = 200
;  varname = "Z3"
;  level = 500

  ; name of statistics file
  stats_fname = "perf_KSstats"+varname+".txt"

;-------------------------
; Import dims using 100 year data
;-------------------------

  dir = "/lustre/atlas/proj-shared/cli115/aag/archive/acme.f1850.c5.ne30g16.t5_perf-ensembles/atm/hist/"
  fil = systemfunc("ls -1 "+dir+"*.cam.h0.00*.nc")
  fh   = addfile(fil(0),"r")                 ; Adds files for read access (filelist is an array)

  lat  = fh->lat
  lon  = fh->lon
  lev  = fh->lev

  nlats = dimsizes(lat)
  nlons = dimsizes(lon)
  nlevs = dimsizes(lev)
  wgt = NormCosWgtGlobe(lat)            ; For weighted global avg: high-latitudes have less area

  ;Formatting file to store K-S output table
  table_titles = [/"Variable","K-S AB","K-S BC","K-S AC"/]
  write_table(stats_fname, "w", table_titles, "%s,%s,%s,%s//")

  print ("Calculating PDF, Quantiles, and K-S test for "+varname+level)

;-------------------------
  ;Abby's 100-year run
;-------------------------

  delete(fil)

  print("READING IN dataset 1!")

  dir = "/lustre/atlas/proj-shared/cli115/aag/archive/acme.f1850.c5.ne30g16.t5_perf-ensembles/atm/hist/"
  fil = systemfunc("ls -1 "+dir+"*.cam.h0.0*.nc")
  nfil_A = dimsizes(fil)

  ;Get dimension information from the first file
  time_A       = new ((/ nfil_A /), double)
  annual_A       = new ((/ nfil_A/12 /), float)
  var_A       = new ((/ nfil_A, nlats, nlons /), float)

  do i = 0 , nfil_A-1
    fh = addfile(fil(i),"r")
       var_A(i,:,:) = fh->$varname$(0,{level},:,:)
;    var_A(i,:,:) = fh->$varname$(0,:,:)
    time_A(i) = fh->time
 end do

  vdata_A = wgt_areaave_Wrap( var_A , wgt , 1.0 , 0 )

;-------------------------
  ;Matt's 100-member ensemble
;-------------------------

  delete(fil)
  print("READING IN dataset 2!") 

  dir = "/lustre/atlas/proj-shared/cli115/aag/annual_filenames_100member/"
  fil = systemfunc("ls -1 "+dir+"*.h0.*.nc")
  nfil_B = dimsizes(fil)                       ; # files

  time_B       = new ((/ nfil_B /), double)
  annual_B       = new ((/ nfil_B/12 /), float)
  var_B       = new ((/ nfil_B, nlats, nlons /), float)

  do i = 0 , nfil_B-1
    fh = addfile(fil(i),"r")
       var_B(i,:,:) = fh->$varname$(0,{level},:,:)
;    var_B(i,:,:) = fh->$varname$(0,:,:)
    time_B(i) = fh->time
  end do

;print(time_B)
;do t=0,dimsizes(time_B)-1
;   if (any(var_B(t,:,:) .le. 0))
;      ;print(time_B(t))
;   end if
;end do  

  vdata_B = wgt_areaave_Wrap( var_B , wgt , 1.0 , 0 )

;-------------------------
  ;Abby's 5 20-year runs
;-------------------------

  delete(fil)
  print("READING IN dataset 3!")

  dir = "/lustre/atlas/proj-shared/cli115/aag/annual_filenames_20yr/"
  fil = systemfunc("ls -1 "+dir+"*.h0.*.nc")
  nfil_C = dimsizes(fil)                       ; # files

  time_C       = new ((/ nfil_C /), double)
  annual_C       = new ((/ nfil_C/12 /), float)
  var_C       = new ((/ nfil_C, nlats, nlons /), float)

  do i = 0 , nfil_C-1
    fh = addfile(fil(i),"r")
       var_C(i,:,:) = fh->$varname$(0,{level},:,:)
;    var_C(i,:,:) = fh->$varname$(0,:,:)
    time_C(i) = fh->time
  end do


  vdata_C = wgt_areaave_Wrap( var_C , wgt , 1.0 , 0 )


  print("FINISHED READING IN THE DATA!")
  delete(fil)

;----------------------------------------------------------------------------------------
; Statistical comparisons between the three data sets
;----------------------------------------------------------------------------------------

;Average, min, and max

  avg_A = avg(vdata_A)
  min_A = min(vdata_A)
  max_A = max(vdata_A)

  avg_B = avg(vdata_B)
  min_B = min(vdata_B)
  max_B = max(vdata_B)

  avg_C = avg(vdata_C)
  min_C = min(vdata_C)
  max_C = max(vdata_C)

  print(avg_A + "  " + avg_B +"  "+ avg_C)
  print(min_A + "  " + min_B +"  "+ min_C)
  print(max_A + "  " + max_B +"  "+ max_C)

;Annual averages of monthly files
  annual_A = annWgtAvg(vdata_A,time_A,nfil_A)
  annual_B = annWgtAvg(vdata_B,time_B,nfil_B)
  annual_C = annWgtAvg(vdata_C,time_C,nfil_C)

;Probability distribution functions of annual global avgs
  hist_A  = pdfx(annual_A,nbins,opt)
  hist_B  = pdfx(annual_B,nbins,opt)
  hist_C  = pdfx(annual_C,nbins,opt)

;Probability distribution functions of monthly global avgs
;  hist_A  = pdfx(vdata_A,nbins,opt)
;  hist_B  = pdfx(vdata_B,nbins,opt)
;  hist_C  = pdfx(vdata_C,nbins,opt)

  bins_array = new( (/ 3 , nbins /) , double )
  hist_array = new( (/ 3 , nbins /) , double )
  bins_array(0,:) = hist_A@bin_center
  bins_array(1,:) = hist_B@bin_center
  bins_array(2,:) = hist_C@bin_center
  hist_array(0,:) = hist_A
  hist_array(1,:) = hist_B
  hist_array(2,:) = hist_C

; KS test: determines if two sets of data are from the same distribution
; Comparing the three sets of global average data
  ksA_B = kolsm2_n(vdata_A,vdata_B, 0)
  ksB_C = kolsm2_n(vdata_B,vdata_C, 0)
  ksA_C = kolsm2_n(vdata_A,vdata_C, 0)

;List of KS values and variable name, writes to table in LIFO order
  KS_values = [/""/]
  ListPush(KS_values, ksA_C)
  ListPush(KS_values, ksB_C)
  ListPush(KS_values, ksA_B)
  ListPush(KS_values, varname)
  write_table(stats_fname, "a", (/KS_values/), "%s,%5.3g,%5.3g,%5.3g//")

;Calculate quantiles of monthly data
  quantiles_A = quantileCalc(vdata_A, nbins)
  quantiles_B = quantileCalc(vdata_B, nbins)
  quantiles_C = quantileCalc(vdata_C, nbins)

;----------------------------------------------------------------------------------------
; Plotting Probability Distribution Function and QQ plots
;----------------------------------------------------------------------------------------

  ; labels for plots
  l1 = "1 100-yr, average = "+avg_A
  l2 = "100 1-yr, average = "+avg_B
  l3 = "5 20-yr, average = "+avg_C
  labels = (/l1,l2,l3/)

  wks          = gsn_open_wks("ps","ensembles_histogram_combined_"+varname)
  res          = True

  res@tiMainString           = vdata_A@long_name + " (" + vdata_A@units + ")"
  res@tiXAxisString          = "Annual Global Average Value"
  res@tiXAxisFontHeightF     = 0.020
  res@tiYAxisFontHeightF     = 0.020
  res@gsnScale               = True        ; force text scaling
  res@gsnMaximize            = True
  res@tmLabelAutoStride      = True
  res@tmXTOn                 = False
  res@tmYROn                 = False
  res2                       = res            ; plot settings for qq plots, same up to here
  res@xyMarkLineMode         = "MarkLines"
  res@tiYAxisString          = "Frequency"
  res@xyLineColors           = (/"black","red","blue"/)
  res@xyLineThicknesses      = (/2,2,2/)
  res@pmLegendDisplayMode    = "Always"
  res@xyExplicitLegendLabels = labels

  ;pdf of annual avg for variable for all three datasets
  plot_pdf = gsn_csm_xy( wks , bins_array(0,:) , hist_array , res )   ; histogram of restom for monthly averages

  ; plot options for comparing quantiles (e.g. percentiles) of two data sets
  res2@xyMarkLineModes       = "Markers"                ; choose which have markers
  res2@xyMarkers             =  16                      ; choose type of marker  
  res2@xyMarkerColor         = "blue"                    ; Marker color
  res2@gsnDraw               = False
  res2@gsnFrame              = False

  ; Creating uniform x and y axes for QQ plots
  max_value = max((/max(quantiles_A),max(quantiles_B)/))
  min_value = min((/min(quantiles_A),min(quantiles_B)/))
  res2@trYMinF               = min_value
  res2@trXMinF               = min_value
  res2@trYMaxF               = max_value
  res2@trXMaxF               = max_value

  ; Quantile-quantile plots for graphical comparison of data
  plot_qq = new ( 3, "graphic")

  ; quantile-quantile plot of dataset A vs B
  res2@tiXAxisString         = "1 100-year simulation"
  res2@tiYAxisString         = "100 1-year simulations"
  plot_qq(0) = gsn_csm_xy( wks , quantiles_A, quantiles_B, res2 ) ;pdf 

  ; quantile-quantile plot of dataset A vs C
  max_value = max((/max(quantiles_A),max(quantiles_C)/))
  min_value = min((/min(quantiles_A),min(quantiles_C)/))
  res2@trYMinF               = min_value
  res2@trXMinF               = min_value
  res2@trYMaxF               = max_value
  res2@trXMaxF               = max_value
  res2@tiXAxisString         = "1 100-year simulation"
  res2@tiYAxisString         = "5 20-year simulations"
  plot_qq(1) = gsn_csm_xy( wks , quantiles_A, quantiles_C, res2 ) ;pdf

  ; quantile-quantile plot of dataset B vs C
  max_value = max((/max(quantiles_B),max(quantiles_C)/))
  min_value = min((/min(quantiles_B),min(quantiles_C)/))
  res2@trYMinF               = min_value
  res2@trXMinF               = min_value
  res2@trYMaxF               = max_value
  res2@trXMaxF               = max_value
  res2@tiXAxisString         = "5 20-year simulations"
  res2@tiYAxisString         = "100 1-year simulations"
  plot_qq(2) = gsn_csm_xy( wks , quantiles_B, quantiles_C, res2 ) ;pdf  
  gsn_panel(wks,plot_qq,(/1,3/),False)


