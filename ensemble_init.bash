# Ensemble generation script: Given a base case, generate an ensemble of a given size using pertlim.
# Abigail Gaddis
# 10/17/2014

#! /bin/bash

# number of ensemble members to generate
nens=10
scripts_dir="/ccs/home/aag/ACME/scripts/"
ens_dir="/ccs/home/aag/ACME/scripts/perf_ensemble/"
base_dir="/ccs/home/aag/ACME/scripts/f1850.c5.acme.ne30g16/"
# source_nml=base_dir+"user_nl_cam"

# create perturbation parameter values for ensemble members

for i in $(seq 1 $nens)
do
   # change pertlim 
   # Ensemble generation procedure: https://wiki.ucar.edu/display/ccsm/CESM+Large+Ensemble+Planning+Page
   #echo "$i"
   PERTLIM_PARAMS[$i]=(1+.1*$i)*10^-14
   echo $PERTLIM_PARAMS
   cd $scripts_dir
   pwd
   # clone base case nens times
   member_dir=$ens_dir"f1850.c5.acme.ne30g16.member$i/"
   # echo $member_dir
   #./create_clone -case $ens_dir -clone $base_dir
   # add pertlim line to all user_cam_nl files
   cd $member_dir
   echo "pertlim = "$PERTLIM_PARAMS[$i] >> user_nl_cam

   # serially configure cases - or could use MPI on the loop?
   # ./cesm_setup -cleanall
   # ./cesm_setup
   # serially build cases
   # ./*.clean_build
   # ./*.build
  # climate train as qsub?
done


