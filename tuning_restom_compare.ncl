;========================================================================================
; Compares top of the model energy balance for several simulations by calculating the
; difference between longwave and shortwave top of model fluxes, taking the global
; weighted average, and plotting it as a time series. The time average is also noted
; on the plot. Used for tuning comparisons. 
;
; Abigail L Gaddis 3/24/15
;=======================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------------------------------------------
; User settings
;----------------------------------------------------------------------------------------

;   casename = "acme.f1850.c5.ne30g16_perf-sensitivity"
   casename = "acme.f1850.c5.ne30g16.t4_perf-ensembles"
   casename2 = "acme.f1850.c5.ne30g16.t5_perf-ensembles"
;   casename2 = "acme.f1850.c5.ne30g16.t1_perf-sensitivity"
   casename3 = "acme.f1850.c5.ne30g16.t2_perf-sensitivity"
   casename4 = "acme.f1850.c5.ne30g16.t3_perf-sensitivity"   
   casename5 = "acme.f1850.c5.ne30g16_perf-ensembles"

   outputfile = "compare_restom"
   dir = "/lustre/atlas/proj-shared/cli106/aag/archive/"+casename+"/atm/"
   fil = systemfunc("cd "+dir+"  ; ls *cam*h0*")
   filelist = addfile(dir+fil, "r")

   dir = "/lustre/atlas/proj-shared/cli106/aag/archive/"+casename2+"/atm/"
   fil = systemfunc("cd "+dir+"  ; ls *cam*h0*")
   filelist2 = addfile(dir+fil, "r")   
   
   dir = "/lustre/atlas/proj-shared/cli106/aag/archive/"+casename3+"/atm/"
   fil = systemfunc("cd "+dir+"  ; ls *cam*h0*")
   filelist3 = addfile(dir+fil, "r")

   dir = "/lustre/atlas/proj-shared/cli106/aag/archive/"+casename4+"/atm/"
   fil = systemfunc("cd "+dir+"  ; ls *cam*h0*")
   filelist4 = addfile(dir+fil, "r")

   dir = "/lustre/atlas/proj-shared/cli106/aag/archive/"+casename5+"/atm/"
   fil = systemfunc("cd "+dir+"  ; ls *cam*h0*")
   filelist5 = addfile(dir+fil, "r")

;----------------------------------------------------------------------------------------
; Read in variables from files
;----------------------------------------------------------------------------------------

; Read in dimensions from file, compute dimension sizes
   time = filelist->time
   time2 = filelist2->time   
   time3 = filelist3->time
   time4 = filelist4->time
   time5 = filelist5->time

   lat = filelist->lat
   lon = filelist->lon
   lev = filelist->lev
   nmon = dimsizes(time)
   nlats = dimsizes(lat)
   nlons = dimsizes(lon)
   nlevs = dimsizes(lev)

   npts = nmon
   wgt = NormCosWgtGlobe(lat)

   flnt = filelist->FLNT
   fsnt = filelist->FSNT 

   flnt2 = filelist2->FLNT
   fsnt2 = filelist2->FSNT

   flnt3 = filelist3->FLNT
   fsnt3 = filelist3->FSNT

   flnt4 = filelist4->FLNT
   fsnt4 = filelist4->FSNT

   flnt5 = filelist5->FLNT
   fsnt5 = filelist5->FSNT


;----------------------------------------------------------------------------------------
; Calculate global weighted average, time average
;----------------------------------------------------------------------------------------
   
    restom_yearav = new((/ 5, dimsizes(time5)/12 /), float)
    restom = flnt-fsnt
    restom2 = flnt2-fsnt2
    restom3 = flnt3-fsnt3
    restom4 = flnt4-fsnt4
    restom5 = flnt5-fsnt5

    yyyymm = cd_calendar(time, -1)
    yyyymm2 = cd_calendar(time2, -1)
    yyyymm3 = cd_calendar(time3, -1)
    yyyymm4 = cd_calendar(time4, -1)
    yyyymm5 = cd_calendar(time5, -1)
    years = ispan(0,dimsizes(yyyymm5)/12-1,1)

    restom_globav = wgt_areaave_Wrap(restom, wgt, 1.0, 0)
    temp = month_to_annual_weighted(yyyymm,restom_globav,1)
    do i=0,(dimsizes(yyyymm)/12-1)
       restom_yearav(0,i) = temp(i)
    end do
    restom_totav = avg(temp)
delete(temp)

    restom_globav2 = wgt_areaave_Wrap(restom2, wgt, 1.0, 0)
    temp = month_to_annual_weighted(yyyymm2,restom_globav2,1)
    do i=0,(dimsizes(yyyymm2)/12-1)
       restom_yearav(1,i) = temp(i)
    end do
    restom_totav2 = avg(temp)

    restom_globav3 = wgt_areaave_Wrap(restom3, wgt, 1.0, 0)
    temp2 = month_to_annual_weighted(yyyymm3,restom_globav3,1)
    do i=0,(dimsizes(yyyymm3)/12-1)
       restom_yearav(2,i) = temp2(i)
    end do
    restom_totav3 = avg(temp2)

    restom_globav4 = wgt_areaave_Wrap(restom4, wgt, 1.0, 0)
    temp3 = month_to_annual_weighted(yyyymm4,restom_globav4,1)
    do i=0,(dimsizes(yyyymm4)/12-1)
       restom_yearav(3,i) = temp3(i)
    end do
    restom_totav4 = avg(temp3)

    restom_globav5 = wgt_areaave_Wrap(restom5, wgt, 1.0, 0)
    temp4 = month_to_annual_weighted(yyyymm5,restom_globav5,1)
    do i=0,(dimsizes(yyyymm5)/12-1)
       restom_yearav(4,i) = temp4(i)
    end do
    restom_totav5 = avg(temp4)

;----------------------------------------------------------------------------------------
; Create plot
;----------------------------------------------------------------------------------------

   ;create plot environment
   wks          = gsn_open_wks("pdf",outputfile)
   res          = True

;   l1 = "p-s c0=.0035, RESTOM = "+restom_totav
;   l2 = "p-s c0=.0100, RESTOM = "+restom_totav2
   l1 = "p-e c0=0.0100, RESTOM = "+restom_totav
   l2 = "p-e c0=0.0090, RESTOM = "+restom_totav2
   l3 = "p-s c0=0.0110, RESTOM = "+restom_totav3
   l4 = "p-s c0=0.0090, RESTOM = "+restom_totav4
   l5 = "p-e c0=0.0110, RESTOM = "+restom_totav5

   labels = (/l1,l2,l3,l4,l5/)

   res@tiXAxisString            = "Time (years)"
   res@tiXAxisFontHeightF       = 0.020
   res@tiYAxisFontHeightF       = 0.020
   res@gsnScale                 = True        ; force text scaling
   res@gsnMaximize              = True
   res@tmLabelAutoStride        = True
   res@tmXTOn                   = False
   res@tmYROn                   = False
   res@xyMarkLineMode           = "MarkLines"
   res@tiYAxisString            = "Global average RESTOM"
   res@xyLineColors             = (/"black","red","blue","green","orange"/)
   res@xyLineThicknesses        = (/2,2,2,2,2/)
   res@pmLegendDisplayMode      = "Always"
   res@pmLegendSide             = "Bottom"
   res@xyExplicitLegendLabels   = labels

   plot = gsn_csm_xy(wks,years,restom_yearav,res); create plot


