;========================================================================================
; ACME model ensemble comparison script 
;
; 6/6/2015
; Abigail L.Gaddis
;=======================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------------------------------------------
; User settings
;----------------------------------------------------------------------------------------

   outputfile = "restom_20year_pdf_compare"

   dir1 = "/lustre/atlas/proj-shared/cli106/aag/archive/acme.f1850.c5.ne30g16.20yr_a.perf-ensembles/atm/hist/"
   dir2 = "/lustre/atlas/proj-shared/cli106/aag/archive/acme.f1850.c5.ne30g16.20yr_b.perf-ensembles/atm/hist/"
   dir3 = "/lustre/atlas/proj-shared/cli106/aag/archive/acme.f1850.c5.ne30g16.20yr_c.perf-ensembles/atm/hist/"
   dir4 = "/lustre/atlas/proj-shared/cli106/aag/archive/acme.f1850.c5.ne30g16.20yr_d.perf-ensembles/atm/hist/"
   dir5 = "/lustre/atlas/proj-shared/cli106/aag/archive/acme.f1850.c5.ne30g16.20yr_e.perf-ensembles/atm/hist/"
   dirs = (/dir1,dir2,dir3,dir4,dir5/)

;----------------------------------------------------------------------------------------
; Read in variables from files
;----------------------------------------------------------------------------------------

; Read in dimensions from file, compute dimension sizes
   nmon = 20*12                         ;number of months desired: 19 years
   nens = 5
   yyyymm = new((/nmon/), integer)

   restom        = new ((/ nens, nmon    /), float) 
   restom_annual = new ((/ nens, nmon/12 /), float)
   restom_totav = new ((/ nens /), float)

   do i=0,nens-1
      fil = systemfunc("cd "+dirs(i)+"  ; ls acme*h0*")
      tempfile = addfile(dirs(i)+fil(0), "r")
      lat = tempfile->lat
      lon = tempfile->lon
      nlats = dimsizes(lat)
      nlons = dimsizes(lon)
      wgt = NormCosWgtGlobe(lat)
      do t=0,nmon-1
          fh = addfile(dirs(i)+fil(t), "r")
          restom(i,t) = wgt_areaave_Wrap(fh->FSNT - fh->FLNT, wgt , 1.0 , 0 )
          time = fh->time 
          yyyymm(t) = cd_calendar(time, -1)
      end do
      delete(fil)
   end do


;---------------------------------------------------------------------------------------
; Calculate global weighted average, time average
;----------------------------------------------------------------------------------------

    do i=0,nens-1
        ann_restom = month_to_annual_weighted(yyyymm,restom(i,:),1)
        restom_annual(i,:) = ann_restom
        restom_totav(i) = avg(ann_restom)
    end do

;----------------------------------------------------------------------------------------
; Calculate pdf of variable
;----------------------------------------------------------------------------------------
    nbins = 20
    restom_ensemble_pdfs = new((/nens,nbins/), double)
    bin_centers = restom_ensemble_pdfs
    do i=0,nens-1
        restom_yearly_pdf = pdfx(restom_annual(i,:),nbins,False)
        bin_centers(i,:) =  restom_yearly_pdf@bin_center
        restom_ensemble_pdfs(i,:) = restom_yearly_pdf
    end do

;----------------------------------------------------------------------------------------
; Create plot
;----------------------------------------------------------------------------------------

   ;create plot environment
   wks          = gsn_open_wks("pdf",outputfile)
   res          = True

   ;set up variables for plot labels

   l1 = "member a, RESTOM = "+restom_totav(0)
   l2 = "member b, RESTOM = "+restom_totav(1)
   l3 = "member c, RESTOM = "+restom_totav(2)
   l4 = "member d, RESTOM = "+restom_totav(3)
   l5 = "member e, RESTOM = "+restom_totav(4)

   labels = (/l1,l2,l3,l4,l5/)

   res@tiXAxisFontHeightF       = 0.020
   res@tiYAxisFontHeightF       = 0.020
   res@tiXAxisString            = "Global Annual Average Top of Model Energy Balance"
   res@tiYAxisString            = "Frequency"
   res@gsnScale                 = True        ; force text scaling
;   res@gsnMaximize              = True
   res@tmLabelAutoStride        = True
   res@tmXTOn                   = False
   res@tmYROn                   = False

;   res@pmLegendDisplayMode      = "Always"
;   res@pmLegendSide             = "Bottom"

   do i=0,nens-1 
       res@gsnRightString = labels(i)
       plot_pdf = gsn_csm_xy (wks,bin_centers(i,:), restom_ensemble_pdfs(i,:), res)
   end do 

res@gsnRightString = "All pdfs vs bin centering of member a"
plot_pdf = gsn_csm_xy (wks,bin_centers(0,:), (/restom_ensemble_pdfs/), res)

