#!/bin/bash 

# Creates links to output files from different directories, useful for analyzing short ensembles

# directory for symlink output
cd /lustre/atlas/proj-shared/cli106/aag

#general format of casenames
casename=titan_test_0

#All ensemble cases that ran - some did not successfully finish
run_cases=(/lustre/atlas/proj-shared/cli106/imn/test_ensembles/$casename*)
#ensid_list=($(seq -f "%04.0f" 1 100))
#Test if any netcdf files were output in each directory, add them to successful files list

for i in "${run_cases[@]}"; do
   #list monthly atmosphere output and pipe the list to null
   ls $i/run/*cam.h0.0002-11.nc &>/dev/null
   #if last month of atmosphere output file exists, add directory to successful dirs list
   if [ $? -eq 0 ];
      then
         success_cases+=($i)
         success_dirs+=(${i##/*/})
         
   fi
done

#printf '%s\n' ${success_cases[@]}
#printf '%s\n' ${success_dirs[@]}
nfols=${#success_cases[@]}
#printf '%s\n' ${nfols[@]}
#echo
#echo $nfols
#echo
#echo ${success_cases[0]}
#echo ${success_cases[1]}
#echo ${success_cases[$nfols-2]}
#echo ${success_cases[$nfols-1]}
#echo
#echo ${success_dirs[0]}
#echo ${success_dirs[1]}
#echo ${success_dirs[$nfols-2]}
#echo ${success_dirs[$nfols-1]}
#echo

ensemble_list=($(seq -f "%04.0f" ${nfols}))

#Create directory for symlinks
dirname=/lustre/atlas/proj-shared/cli106/aag/annual_filenames_100member
rm -rf $dirname
mkdir $dirname
glob_ind=1

cd ${dirname}
cd /lustre/atlas/proj-shared/cli106/aag/annual_filenames_100member
for (( i=0; i<${nfols}-1; i++ )); do
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0001-12.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}-1]}-12.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-01.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-01.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-02.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-02.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-03.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-03.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-04.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-04.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-05.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-05.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-06.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-06.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-07.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-07.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-08.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-08.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-09.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-09.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-10.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-10.nc
   ln -s $( ls ${success_cases[${i}]}/run/${casename}*.cam.h0.0002-11.nc ) ./titan_test.cam.h0.${ensemble_list[${glob_ind}]}-11.nc
   glob_ind=$( echo "${glob_ind}+1" | bc )
done




