;
; Function to remove bottom 5% of data. We found a bug in the inline model data interpolation
; that optionally was turned on in generating our 3 data sets. Spurious -999 values were 
; introduced and interpolated. These values often comprise the bottom 5% of the data. The lowest
; quantile of data is removed using this script. 
;
; Abigail L. Gaddis
; 10/14/2015


function remove_lowest_5pct(data)
   local data, data_fixed, data_1D, nbins, quantiles
   begin 
      ; Reshapes data into a 1D series of numbers = we don't care about the order
      data_1D = ndtooned(data)
      ; Calculate quantiles of data binned at 5% intervals (20 bins = 100%)
      nbins = 20
      quantiles = quantileCalc(data_1D,nbins)
      print(quantiles)
      ; Select lowest 5% quantile value
      bad_value = quantiles(0)
      ; Print value of lowest quantile
      print("Data less than this value set to fill value: "+bad_value)
      print("Default data fill value: " + data@_FillValue)
      ; Where the original data is less than or equal to the bad value, set it to _FillValue
      data_fixed = where(data .le. bad_value, data@_FillValue, data)
      return data_fixed 
   end

