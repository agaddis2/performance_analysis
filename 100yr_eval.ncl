;========================================================================================
; ACME model ensemble comparison script 
;
; 6/6/2015
; Abigail L.Gaddis
;=======================================================================================

  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;-------------------------
; User specified settings
;-------------------------

  varnames = (/"TS", "PS","PRECC","PRECL","QREFHT","CLDTOT","RELHUM", "U10"/)
  nbins = 25
;  min_bin = 284
;  max_bin = 288

;-------------------------
; Import dims using 100 year data
;-------------------------

  dir = "/lustre/atlas/proj-shared/cli106/aag/archive/acme.f1850.c5.ne30g16.t5_perf-ensembles/atm/hist/"
  fil = systemfunc("ls -1 "+dir+"*.cam.h0.00*.nc")

;print(fil)

  nfil = dimsizes(fil)                       ; # files
print(nfil)
  fh   = addfile(fil(0),"r")                 ; Adds files for read access (filelist is an array)

; If you want to know what variables are available, uncomment
;all_varnames = getfilevarnames(fh)
;print(all_varnames)

  lat  = fh->lat
  lon  = fh->lon
  lev  = fh->lev
  nlats = dimsizes(lat)
  nlons = dimsizes(lon)
  nlevs = dimsizes(lev)
  wgt = NormCosWgtGlobe(lat)                        ; For weighted global average because high-latitudes have lower area

  varname = varnames(0)

  ;Get dimension information from the first file
  var_A    = new ((/ nfil, nlats, nlons/), float)
;  vdata_A = var_A_zonal(:,0)
  time = new ((/ nfil/), double)

  print("READING IN THE DATA!")
  do i = 0 , nfil-1
    fh = addfile(fil(i),"r")
    var_A(i,:,:) = fh->$varname$(0,:,:)
    time(i) = fh->time
    if (i%100 .eq. 0) then
      print(((i*1.0)/(nfil-1))*100+"% Complete")
    end if
  end do
  print("FINISHED READING IN THE DATA!")

  var_A!0 = "time"
  var_A&time = time
  var_A_zonal = dim_avg_n_Wrap(var_A, 2) ; averaging over dim(1), longitude, to get zonal average
  vdata_A = wgt_areaave_Wrap( var_A , wgt , 1.0 , 0 )
  climo_A = wgt_areaave_Wrap(clmMonTLL(var_A), wgt, 1.0, 0)
  printVarSummary(climo_A)

  opt = False
  hist_A  = pdfx(vdata_A,nbins,opt)

;----------------------------------------------------------------------------------------
; Statistical comparisons between the three data sets
;----------------------------------------------------------------------------------------

  bins = hist_A@bin_center

; Calculate quantiles
   nquants = 100       ; number of desired quantiles, 100 gives percentiles
   percents = fspan(0,nquants-1,nquants)/(nquants)
   quantiles_A = new((/nquants/), float)
   
; Calculate the size and sort each dataset
   size_A = dimsizes(vdata_A)
   vdata_A_sorted = vdata_A
   sort_vector_A = dim_pqsort(vdata_A_sorted, 2)

; Calculate the index and value of the quantiles
   do i = 0, nquants-1
      ; calculate index of the quantile
      p_quant_A = percents(i)*size_A

      ; if index A is an integer, quantile is value at that index
      if (p_quant_A .eq. floor(p_quant_A)) then      ; p_quant_A smallest integer value <= p_quant_A
         quantiles_A(i) = vdata_A_sorted(floattointeger(p_quant_A))
      else
      ; if index A has remainder, interpolate nearby values to get quantile
         x0 = floattointeger(floor(p_quant_A))
         x1 = floattointeger(ceil(p_quant_A))
         y0 = vdata_A_sorted(x0)
         y1 = vdata_A_sorted(x1)
         quantiles_A(i) = y0 + (y1 - y0) * ((p_quant_A - x0)/(x1 - x0)) 
      end if
   end do
  
  time = time/365 
  annual_av = month_to_annual(vdata_A,1)
  
;----------------------------------------------------------------------------------------
; Create plot
;----------------------------------------------------------------------------------------
  wks          = gsn_open_wks("pdf","100yr_eval"+varname)
  res          = True

  res@tiMainString             = vdata_A@long_name + " (" + vdata_A@units + ")"
  res@tiXAxisString            = "Monthly Global Average Value"
  res@tiXAxisFontHeightF       = 0.020
  res@tiYAxisFontHeightF       = 0.020
  res@gsnScale                 = True        ; force text scaling
  res@gsnMaximize              = True
  res@tmLabelAutoStride        = True
  res@tmXTOn                   = False
  res@tmYROn                   = False
  res2                         = res            ; plot settings for qq plots, same up to here
  res@xyMarkLineMode           = "MarkLines"
  res@tiYAxisString            = "Frequency"
  res@xyLineColors             = (/"black","red","blue"/)
  res@xyLineThicknesses        = (/2,2,2/)
  plot_pdf = gsn_csm_xy( wks , bins , hist_A , res )   ; histogram of restom for monthly averages

; Time plots
  rest = True
  res@tiMainString             = "100 year single simulation"
  rest@tiXAxisString            = "Time (years)"
  rest@tiYAxisString            = vdata_A@long_name + " (" + vdata_A@units + ")"

  rescn = True
  rescn@tiMainString             = vdata_A@long_name+ " zonal average"
  rescn@tiYAxisString            = "Latitude (degrees)"
  ;rescn@gsnScale                 = True        ; force text scaling

  rescn@gsnSpreadColors          = True
  rescn@cnLinesOn                = False        ; True is default
  rescn@cnLineLabelsOn           = False        ; True is default
  rescn@lbLabelAutoStride        = True         ; auto stride on labels

  plot_zonal = gsn_csm_contour(wks, var_A_zonal(lat|:,time|:),rescn)
  plot_timeslice = gsn_csm_contour_map(wks, var_A(0,:,:), False)

  rest@tiMainString             = "100 year run monthly global average"
  rest@xyMarkLineModes   = "Markers"
  plot_time = gsn_csm_xy(wks, time, vdata_A, rest)

  rest@tiMainString             = "100 year run annual global average"
  years = ispan(0,nfil/12 -1,1)
  plot_time2 = gsn_csm_xy(wks, years, annual_av, rest)

  rest@tiMainString             = "100 year run, first year"
  plot_time3 = gsn_csm_xy(wks, time(0:11), vdata_A(0:11), rest)

  rest@tiMainString             = "100 year run, monthly climatology"
  plot_time4 = gsn_csm_xy(wks, climo_A&month, climo_A, rest)

