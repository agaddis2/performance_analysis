;========================================================================================
; ACME model ensemble comparison script 
;
; 3/6/2015
; Abigail L.Gaddis
;=======================================================================================

  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;-------------------------
; User specified settings
;-------------------------

  varnames = (/"TS", "PS","PRECC","PRECL","QREFHT","CLDTOT","QFLX", "U10"/)

  ;bin size options for histogram
  opt = False
  ;opt = True
  ;opt@bin_min = min_bin
  ;opt@bin_max = max_bin
  nbins = 25
  ;min_bin = 284
  ;max_bin = 288

;-------------------------
; Import dims using 100 year data
;-------------------------

  dir = "/lustre/atlas/proj-shared/cli115/aag/archive/acme.f1850.c5.ne30g16.t5_perf-ensembles/atm/hist/"
  fil = systemfunc("ls -1 "+dir+"*.cam.h0.00*.nc")
  fh   = addfile(fil(0),"r")                 ; Adds files for read access (filelist is an array)

; If you want to know what variables are available, uncomment
;all_varnames = getfilevarnames(fh)
;print(all_varnames)

  lat  = fh->lat
  lon  = fh->lon
  lev  = fh->lev
  nlats = dimsizes(lat)
  nlons = dimsizes(lon)
  nlevs = dimsizes(lev)
  wgt = NormCosWgtGlobe(lat)                        ; For weighted global average because high-latitudes have lower area

  varname = varnames(6)

  ; for determining annual averages
  days_per_month = (/ 31,28,31,30,31,30,31,31,30,31,30,31 /) 

;-------------------------
  ;Abby's 100-year run
;-------------------------


  dir = "/lustre/atlas/proj-shared/cli115/aag/archive/acme.f1850.c5.ne30g16.t5_perf-ensembles/atm/hist/"
  fil = systemfunc("ls -1 "+dir+"*.cam.h0.00*.nc")
  nfil = dimsizes(fil)
print(dimsizes(fil))


  ;Get dimension information from the first file
  var_A_zonal    = new ((/ nfil, nlats/), float)
  annual_A       = new ((/ nfil/12 /), float)

  time = ispan(0,nfil-1,1)
  var_A_zonal!0 = "time"
  var_A_zonal&time = time
  vdata_A = var_A_zonal(:,0)

  print("READING IN THE DATA!")
  do i = 0 , nfil-1
    fh = addfile(fil(i),"r")
    var_A = fh->$varname$(0,:,:)

    var_A_zonal(i,:) = dim_avg_n_Wrap(var_A, 1) ; averaging over dim(1), longitude, to get zonal average
    vdata_A(i) = wgt_areaave_Wrap( var_A , wgt , 1.0 , 0 )
    if (i%100 .eq. 0) then
      print(((i*1.0)/(nfil-1))*100+"% Complete")
    end if
  end do
  print("FINISHED READING IN THE DATA!")

  avg_A = avg(vdata_A)
  min_A = min(vdata_A)
  max_A = max(vdata_A)

  ; annual averages for A dataset
  do i = 0 , nfil/12-1
    annual_A(i) = 0.
    do j = 0 , 11
      annual_A(i) = annual_A(i) + days_per_month(j)*vdata_A(i*12+j)
    end do
    annual_A(i) = annual_A(i) / 365
  end do

  hist_A  = pdfx(annual_A,nbins,opt)
  var_A_zonal!0 = "time"

;-------------------------
  ;Matt's 100-member ensemble
;-------------------------

  delete(fil)
 
  dir = "/lustre/atlas/proj-shared/cli115/aag/annual_filenames_100member/"
  fil = systemfunc("ls -1 "+dir+"*.h0.*.nc")
  nfil = dimsizes(fil)                       ; # files
print(dimsizes(fil))

  vdata_B        = new ((/ nfil /), float) 
  var_B_zonal    = new ((/ nfil, nlats/), float)
  annual_B       = new ((/ nfil/12 /), float)

  print("READING IN THE DATA!")
  do i = 0 , nfil-1

    fh = addfile(fil(i),"r")
    var_B = fh->$varname$(0,:,:)
    var_B_zonal(i,:) = dim_avg_n_Wrap(var_B, 1) ; averaging over dim(1), longitude, to get zonal average    
    vdata_B(i) = wgt_areaave_Wrap( var_B , wgt , 1.0 , 0 )
    if (i%100 .eq. 0) then
      print(((i*1.0)/(nfil-1))*100+"% Complete")
    end if
  end do
  print("FINISHED READING IN THE DATA!")

  avg_B = avg(vdata_B)
  min_B = min(vdata_B)
  max_B = max(vdata_B)

  ; annual averages for B dataset
  do i = 0 , nfil/12-1
    annual_B(i) = 0.
    do j = 0 , 11
      annual_B(i) = annual_B(i) + days_per_month(j)*vdata_B(i*12+j)
    end do
    annual_B(i) = annual_B(i) / 365
  end do

  hist_B  = pdfx(annual_B,nbins,opt)
  var_B_zonal!0 = "time"

;-------------------------
  ;Abby's 5 20-year runs
;-------------------------

  delete(fil)

  dir = "/lustre/atlas/proj-shared/cli115/aag/annual_filenames_20yr/"
  fil = systemfunc("ls -1 "+dir+"*.h0.*.nc")
  nfil = dimsizes(fil)                       ; # files

print(dimsizes(fil))

  vdata_C        = new ((/ nfil    /), float) 
  var_C_zonal    = new ((/ nfil, nlats/), float)
  annual_C       = new ((/ nfil/12 /), float)

  print("READING IN THE DATA!")
  do i = 0 , nfil-1
    fh = addfile(fil(i),"r")
    var_C = fh->$varname$(0,:,:)
    var_C_zonal(i,:) = dim_avg_n_Wrap(var_C, 1) ; averaging over dim(1), longitude, to get zonal average
    vdata_C(i) = wgt_areaave_Wrap( var_C , wgt , 1.0 , 0 )
    if (i%100 .eq. 0) then
      print(((i*1.0)/(nfil-1))*100+"% Complete")
    end if
  end do
  print("FINISHED READING IN THE DATA!")

  avg_C = avg(vdata_C)
  min_C = min(vdata_C)
  max_C = max(vdata_C)

  ; annual averages for C dataset
  do i = 0 , nfil/12-1
    annual_C(i) = 0.
    do j = 0 , 11
      annual_C(i) = annual_C(i) + days_per_month(j)*vdata_C(i*12+j)
    end do
    annual_C(i) = annual_C(i) / 365
  end do

  hist_C  = pdfx(annual_C,nbins,opt)
  var_C_zonal!0 = "time"

;----------------------------------------------------------------------------------------
; Statistical comparisons between the three data sets
;----------------------------------------------------------------------------------------

  bins_array = new( (/ 3 , nbins /) , double )
  hist_array = new( (/ 3 , nbins /) , double )
  bins_array(0,:) = hist_A@bin_center
  bins_array(1,:) = hist_B@bin_center
  bins_array(2,:) = hist_C@bin_center
  hist_array(0,:) = hist_A
  hist_array(1,:) = hist_B
  hist_array(2,:) = hist_C

  ; KS test - are these ensembles the same?
  ksA_B = kolsm2_n(vdata_A,vdata_B, -1)
  ksB_C = kolsm2_n(vdata_B,vdata_C, -1)
  ksA_C = kolsm2_n(vdata_A,vdata_C, -1)

  print(ksA_B)
  print(ksB_C)
  print(ksA_C)

; Calculate quantiles
   nquants = nbins       ; number of desired quantiles, 100 gives percentiles
   percents = fspan(0,nquants-1,nquants)/(nquants)
   
   quantiles_A = new((/nquants/), float)
   quantiles_B = quantiles_A
   quantiles_C = quantiles_A
   
; Calculate the size and sort each dataset
   size_A = dimsizes(annual_A)
   sort_vector_A = dim_pqsort(annual_A, 2)
   size_B = dimsizes(annual_B)
   sort_vector_B = dim_pqsort(annual_B, 2)
   size_C = dimsizes(annual_C)
   sort_vector_C = dim_pqsort(annual_C, 2)

; Calculate the index and value of the quantiles
   do i = 0, nquants-1
      ; calculate index of the quantile
      p_quant_A = percents(i)*size_A
      p_quant_B = percents(i)*size_B
      p_quant_C = percents(i)*size_C

      ; if index A is an integer, quantile is value at that index
      if (p_quant_A .eq. floor(p_quant_A)) then      ; p_quant_A smallest integer value <= p_quant_A
         quantiles_A(i) = annual_A(floattointeger(p_quant_A))
      else
      ; if index A has remainder, interpolate nearby values to get quantile
         x0 = floattointeger(floor(p_quant_A))
         x1 = floattointeger(ceil(p_quant_A))
         y0 = annual_A(x0)
         y1 = annual_A(x1)
         quantiles_A(i) = y0 + (y1 - y0) * ((p_quant_A - x0)/(x1 - x0)) 
      end if

      ; if index B is an integer, quantile is value at that index
      if (p_quant_B .eq. floor(p_quant_B)) then      ; p_quant_B smallest integer value <= p_quant_B
         quantiles_B(i) = vdata_B(floattointeger(p_quant_B))
      else
      ; if index B has remainder, interpolate nearby values to get quantile
         x0 = floattointeger(floor(p_quant_B))
         x1 = floattointeger(ceil(p_quant_B))
         y0 = (/annual_B(x0)/)
         y1 = (/annual_B(x1)/)
         quantiles_B(i) = y0 + (y1 - y0) * ((p_quant_B - x0)/(x1 - x0))
      end if

      ; if index C is an integer, quantile is value at that index
      if (p_quant_C .eq. floor(p_quant_C)) then      ; p_quant_C smallest integer value <= p_quant_C
         quantiles_C(i) = annual_C(floattointeger(p_quant_C))
      else
      ; if index C has remainder, interpolate nearby values to get quantile
         x0 = floattointeger(floor(p_quant_C))
         x1 = floattointeger(ceil(p_quant_C))
         y0 = (/annual_C(x0)/)
         y1 = (/annual_C(x1)/)
         quantiles_C(i) = y0 + (y1 - y0) * ((p_quant_C - x0)/(x1 - x0))
      end if
   end do

;----------------------------------------------------------------------------------------
; Create plot
;----------------------------------------------------------------------------------------

  ; labels for plots
  l1 = "1 100-yr, average = "+avg_A
  l2 = "100 1-yr, average = "+avg_B
  l3 = "5 20-yr, average = "+avg_C
  labels = (/l1,l2,l3/)

  wks          = gsn_open_wks("pdf","ensembles_histogram_combined_"+varname)
  res          = True

  res@tiMainString             = vdata_A@long_name + " (" + vdata_A@units + ")"
  res@tiXAxisString            = "Monthly Global Average Value"
  res@tiXAxisFontHeightF       = 0.020
  res@tiYAxisFontHeightF       = 0.020
  res@gsnScale                 = True        ; force text scaling
  res@gsnMaximize              = True
  res@tmLabelAutoStride        = True
  res@tmXTOn                   = False
  res@tmYROn                   = False
  res2                         = res            ; plot settings for qq plots, same up to here
  res@xyMarkLineMode           = "MarkLines"
  res@tiYAxisString            = "Frequency"
  res@xyLineColors             = (/"black","red","blue"/)
  res@xyLineThicknesses        = (/2,2,2/)
  res@pmLegendDisplayMode      = "Always"
  res@xyExplicitLegendLabels   = labels

  ;pdf of annual avg for variable for all three datasets
  plot_pdf = gsn_csm_xy( wks , bins_array(0,:) , hist_array , res )   ; histogram of restom for monthly averages

  ; quantile-quantile plot of dataset A vs B
  res2@xyMarkLineModes   = "Markers"                ; choose which have markers
  res2@xyMarkers         =  16                      ; choose type of marker  
  res2@xyMarkerColor     = "blue"                    ; Marker color

  res2@tiXAxisString            = "1 100-year simulation"
  res2@tiYAxisString            = "100 1-year simulations"
  plot_qq1 = gsn_csm_xy( wks , quantiles_A, quantiles_B, res2 ) ;pdf 

  ; quantile-quantile plot of dataset A vs C
  res2@tiXAxisString            = "1 100-year simulation"
  res2@tiYAxisString            = "5 20-year simulations"
  plot_qq2 = gsn_csm_xy( wks , quantiles_A, quantiles_C, res2 ) ;pdf

  ; quantile-quantile plot of dataset B vs C
  res2@tiXAxisString            = "5 20-year simulations"
  res2@tiYAxisString            = "100 1-year simulations"
  plot_qq3 = gsn_csm_xy( wks , quantiles_B, quantiles_C, res2 ) ;pdf



