;---------------------------------------------------------------------------------
; A set of statistical functions to be called by other programs
;---------------------------------------------------------------------------------

; Computes the annual weighted average of monthly noleap data
; Assumptions: data begins with January, data has time dimension 
   
 function annWgtAvg(var_data, time, nfil)
   local var_data, nfil, time, annual_avg, days_per_month
   begin
      days_per_month = (/ 31,28,31,30,31,30,31,31,30,31,30,31 /)
      if (time@calendar .ne. "noleap")
         print ("Cannot support this calendar type.")
         return (-1)
      else
        annual_avg = new((/nfil/12/), typeof(var_data))
        do i = 0 , nfil/12-1
          annual_avg(i) = 0.
          do j = 0 , 11
            annual_avg(i) = annual_avg(i) + days_per_month(j)*var_data(i*12+j)
          end do
          annual_avg(i) = annual_avg(i) / 365
        end do
        copy_VarAtts(var_data,annual_avg)
        return(annual_avg)
      end if
   end   

; Computes the quantiles of a dataset, given a number of bins

 function quantileCalc(var_data,nbins)
   local var_data, nbins, nquants, percents, quantiles, dsize, sort_vector, p_quant
   begin

   ; Calculate percentages for each quantile
      nquants = nbins       ; number of desired quantiles, 100 gives percentiles
      percents = fspan(0,nquants-1,nquants)/(nquants)
      quantiles = new((/nquants/), float)

   ; Calculate the size and sort each dataset
      dsize = dimsizes(var_data)
      ; Checking that the data is 1 dimensional
      rank = dimsizes(dsize)
      if (rank .gt. 1) then
         print("Variable size not supported by quantileCalc. Convert to 1D.")
         return -1
      end if
      ; This sorts the original var_data (!!) and returns the sort vector
      sort_vector = dim_pqsort(var_data, 2)

   ; Calculate the index and value of the quantiles
      do i = 0, nquants-1
         ; calculate index of the quantile
         p_quant = percents(i)*dsize

         ; if index is an integer, quantile is value at that index
         if (p_quant .eq. floor(p_quant)) then      ; p_quant smallest integer value <= p_quant
            quantiles(i) = var_data(floattointeger(p_quant))
         else
         ; if index has remainder, interpolate nearby values to get quantile
            x0 = floattointeger(floor(p_quant))
            x1 = floattointeger(ceil(p_quant))
            y0 = var_data(x0)
            y1 = var_data(x1)
            quantiles(i) = y0 + (y1 - y0) * ((p_quant - x0)/(x1 - x0))
         end if
      end do
      return(quantiles)
   end

