#!/bin/bash


#if [ 0 -eq 1 ] ; then

cd /lustre/atlas/proj-shared/cli106/aag

casename=acme.f1850c5.ne30g16.1yr.100ens
cases=(/lustre/atlas/proj-shared/cli106/imn/100member_400day_ensemble_data/$casename*)
nfiles=${#cases[@]}
ensid_list=($(seq -f "%04.0f" 1 100))

dirname=annual_filenames_100member
rm -rf $dirname
mkdir -p $dirname
glob_ind=0

for (( i=0; i<${nfiles}; i++ )); do
  if [ "D"$i == "D78" ] ; then continue ;  fi
  if [ "D"$i == "D90" ] ; then continue ;  fi
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0002-01.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-01.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-02.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-02.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-03.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-03.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-04.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-04.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-05.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-05.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-06.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-06.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-07.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-07.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-08.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-08.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-09.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-09.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-10.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-10.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-11.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-11.nc
  ln -s $( ls /${cases[${i}]}/${casename}.????.cam.h0.0001-12.nc ) ${dirname}/cam.h0.${ensid_list[${glob_ind}]}-12.nc
  glob_ind=$( echo "${glob_ind}+1" | bc )
done

#fi


cd /lustre/atlas/proj-shared/cli115/aag

dirname=annual_filenames_20yr
rm -rf $dirname
mkdir -p $dirname

cases=(/lustre/atlas/proj-shared/cli115/aag/archive/acme.f1850.c5.ne30g16.20yr_*)
ndirs=${#cases[@]}
glob_ind=0
ensid_list=($(seq -f "%04.0f" 0 100))
for (( i=0; i<${ndirs}; i++ )); do
  files=(${cases[${i}]}/atm/hist/*.cam.h0.*.nc)
  nfiles=${#files[@]}
  for (( j=0; j<240; j++ )); do
    year=$( echo $( basename ${files[${j}]} ) | cut -d "." -f 9 | cut -d '-' -f 1 )
    month=$( echo $( basename ${files[${j}]} ) | cut -d "." -f 9 | cut -d '-' -f 2 )
    year=$( echo "${year}+${glob_ind}" | bc )
    echo ${ensid_list[${year}]}-${month}
    echo "ln -s ${files[${j}]} ${dirname}/cam.h0.${ensid_list[${year}-1]}-${month}.nc"
    ln -s ${files[${j}]} ${dirname}/cam.h0.${ensid_list[${year}-1]}-${month}.nc
  done
  glob_ind=$( echo "${glob_ind}+19" | bc )
done

